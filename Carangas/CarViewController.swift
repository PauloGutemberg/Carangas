//
//  CarViewController.swift
//  Carangas
//
//  Created by Eric Brito on 21/10/17.
//  Copyright © 2017 Eric Brito. All rights reserved.
//

import UIKit
import Foundation

//para usar web view preciso importar o web kit
//alem disso precisa adicionar o framework do web kit ios 12
import WebKit

class CarViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var lbBrand: UILabel!
    @IBOutlet weak var lbGasType: UILabel!
    @IBOutlet weak var lbPrice: UILabel!
	
	@IBOutlet weak var webView: WKWebView!
	@IBOutlet weak var loading: UIActivityIndicatorView!
	
	var car: Car!
    // MARK: - Super Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		
		//ideal pra trabalhar com moedas seria o number formater
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		title = car.name
		lbBrand.text = car.brand
		lbGasType.text = car.gas
		lbPrice.text = "R$ \(car.price)"
		
		//problema no web view com nomes com spacos?
		//trocar spacos pra +
		let name = (title! + "+" + car.brand).replacingOccurrences(of: " ", with: "+")
		let urlString = "https://www.google.com.br/search?q=\(name)&tbm=isch"
		let url = URL(string: urlString)!
		let request = URLRequest(url: url)
		//para usar os gestos do aparelho e proporcionar uma melhor esperiencia
		webView.allowsBackForwardNavigationGestures = true
		//previsualizar um link
		webView.allowsLinkPreview = true
		
		//webkit tem dois delegates
		//analizar cada um depois
		webView.navigationDelegate = self
		webView.uiDelegate = self
		
		webView.load(request)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		let viewControllerDestino = segue.destination as! AddEditViewController
		viewControllerDestino.car = car
	}

}

extension CarViewController: WKNavigationDelegate, WKUIDelegate{
	//sempre que terminar o carregamento de uma pagina esse metodo vai ser chamado
	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
		loading.stopAnimating()
	}
}
