//
//  RESTCar.swift
//  Carangas
//
//  Created by Paulo Gutemberg on 17/04/19.
//  Copyright © 2019 Eric Brito. All rights reserved.
//

import Foundation

/*Possiveis erros que podem acontecer na closure
obs: o nome é car erro, mas o erros sao genericos
*/


enum CarError{
	case url // nao consegui criar minha url
	case taskError(error: Error) //erro da minha propria tarefa
	case noResponse //se veio nulo, nao tive nem uma resposta
	case noData //se minha data vier nula
	case responseStatusCode(Code: Int)//em que veio o response mais o estatus nao é 200 é outro
	case invalidJSON //posso nao conseguir decodificar meu JSON
}

enum RESTOperation{
	case save
	case update
	case delete
}

class RESTCar {
	
	//URL principal de minha API
	private static let basePath = "https://carangas.herokuapp.com/cars"
	
	//configurando a propria sessao
	private static let configuration: URLSessionConfiguration = {
		/*ephemeral = modo privado de urlsession configurario
		defaut = padraozao, usar cache quando o usuario nao tem mais dados,
		e salva as coisas localmente
		
		background = requisicoes quando o app nao esta sendo utilizado, acesso algum servico na nuvem por exemplo.
		*/
		let config = URLSessionConfiguration.default
		/*config.allowsCellularAccess = false nao permitir o usuario que se acesse utilizando 3g ou 4g*/
		config.allowsCellularAccess = true
		
		//Serve para adicionar um header para aplicacao como por exemplo chaves de apis como jwt
		//para seguranca
		config.httpAdditionalHeaders = ["Content-Type":"application/json"]
		
		//Estou dando 30 segundos para minha requisicao funcionar
		config.timeoutIntervalForRequest = 30.0
		
		//limito a 5 downloads ao mesmo tempo
		config.httpMaximumConnectionsPerHost = 5
		
		// retorna config para configuration
		return config
	}()
	
	//objeto de sessao
	private static let session = URLSession(configuration: configuration)
	//URLSession.shared
	
	
	class func loadBrands(onComplete: @escaping ([Brand]?) -> Void) {
		guard let url = URL(string: "https://fipeapi.appspot.com/api/1/carros/marcas.json") else {
			onComplete(nil)
			return
		}
	
		let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
			if error == nil {
				guard let response = response  as? HTTPURLResponse else {
					onComplete(nil)
					return
					
				}
				if response.statusCode == 200 {
					guard let data = data else {return}
					do{
						let brands = try JSONDecoder().decode([Brand].self , from: data)
						onComplete(brands)
					}catch{
						onComplete(nil)
					}
				}else{
					onComplete(nil)
				}
			}else{
				onComplete(nil)
			}
		}
		dataTask.resume()
	}
	
	/*Utilizando clousure como parametro desse metodo para ele retorna os carros direitinho
	onComplete  () -> void
	*/
	/*Palavra reservada @scaping faz com que o parametro seja retido mesmo com o final do metodo, da uma olhada com calma na anotation @scaping */
	class func loadCars(onComplete: @escaping ([Car]) -> Void , onError: @escaping (CarError) -> Void) {
		//usar guard porque o valor pode vir nulo
		guard let url = URL(string: basePath) else {
			onError(.url)
			
			return
			
		}
		
		//criar tarefa de solicitacao de dados
		/*Data e nosso proprio Json de carros e esta como optional*/
		//esse metodo so cria a tarefa
		
		let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
			/*
			Response - Erros dos Servidor vem aqui
			erros - Erros do App vem aqui
			*/
			if error == nil {
				//deu tudo certo
				guard let response = response  as? HTTPURLResponse else {
					onError(.noResponse)
					return
					
				}
				/* as? HTTPURLResponse da acesso ao status da resposta do servidor */
				if response.statusCode == 200 {
					//Agora com a verificacao do servidor de forma correta o tratamento comeca daqui
					guard let data = data else {return}
					
					do{
						//agora sim posso fazer a conversao do meu data para o array de carros
						let cars = try JSONDecoder().decode([Car].self , from: data)
						
						onComplete(cars)
						
					}catch{
						//print(error.localizedDescription)
						onError(.invalidJSON)
					}
				}else{
					//print("Algo esta errado com meu servidor!")
					onError(.responseStatusCode(Code: response.statusCode))
				}
			}else{
				//deu tudo errado
				onError(.taskError(error: error!))
			}
		}
		//esse metodo faz a solicitacao da tarefa
		dataTask.resume()
	}
	
	class func save(car: Car, onComplete: @escaping (Bool) -> Void){
		applyOperation(car: car, operation: .save, onComplete: onComplete)
	}
	
	class func update(car: Car, onComplete: @escaping (Bool) -> Void){
		applyOperation(car: car, operation: .update, onComplete: onComplete)
	}
	
	class func delete(car: Car, onComplete: @escaping (Bool) -> Void){
		applyOperation(car: car, operation: .delete, onComplete: onComplete)
	}
	
	private class func applyOperation(car: Car, operation: RESTOperation, onComplete: @escaping (Bool) -> Void){
		
		//caso tenha id vai desembrulhado do contrario vai string vazia
		let urlString = basePath + "/" + (car._id ?? "")
		
		guard let url = URL(string: urlString) else {
			onComplete(false)
			return
		}
		
		var httpMethod: String = ""
		var request = URLRequest(url: url)
		
		switch operation {
		case .save:
			httpMethod = "POST"
		case .update:
			httpMethod = "PUT"
		case .delete:
			httpMethod = "DELETE"
	
		}
		
		request.httpMethod = httpMethod
		
		guard let json = try? JSONEncoder().encode(car) else {
			onComplete(false)
			return
		}
		//Alimentando o corpo do meu request com os dados inseridos dos carros
		request.httpBody = json
		
		let dataTask = session.dataTask(with: request) { (data, response, error) in
			if error == nil {
				//desenbrulahando data e response ao mesmo tempo
				guard let response = response as? HTTPURLResponse, response.statusCode == 200, let _ = data else{
					onComplete(false)
					return
				}
				//Se tudo der certo
				onComplete(true)
			}else{
				onComplete(false)
			}
		}
		dataTask.resume()
		
	}
}
