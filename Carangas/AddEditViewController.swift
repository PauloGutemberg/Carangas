//
//  AddEditViewController.swift
//  Carangas
//
//  Created by Eric Brito.
//  Copyright © 2017 Eric Brito. All rights reserved.
//

import UIKit

class AddEditViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tfBrand: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var scGasType: UISegmentedControl!
    @IBOutlet weak var btAddEdit: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!
	
	var car: Car!
	var brands: [Brand] = []
	
	lazy var pickerView: UIPickerView = {
		let pikerView = UIPickerView()
		pikerView.backgroundColor = .white
		pikerView.delegate = self
		pikerView.dataSource = self
		return pikerView
	}()
	
    // MARK: - Super Methods
    override func viewDidLoad() {
        super.viewDidLoad()
		
		if car != nil {
			tfBrand.text = car.brand
			tfName.text = car.name
			tfPrice.text = "\(car.price)"
			scGasType.selectedSegmentIndex = car.gasType
			btAddEdit.setTitle("Alterar carro", for: .normal)
			
		}
		
		let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44))
		toolbar.tintColor = UIColor(named: "main")
		let btCancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
		let btSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		let btDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
		
		toolbar.items = [btCancel, btSpace , btDone]
		tfBrand.inputAccessoryView = toolbar
		
		tfBrand.inputView = pickerView
		loadBrands()
    }
	
	
    // MARK: - IBActions
    @IBAction func addEdit(_ sender: UIButton) {
		
		sender.isEnabled = false
		sender.backgroundColor = .gray
		sender.alpha = 0.5
		loading.startAnimating()
		
		if car == nil {
			//criar um construtor de forma correta para alimentar carro
			car = Car()
		}
		
		//ideal é validar todas essas informacoes
		car.name = tfName.text!
		car.brand = tfBrand.text!
		car.price = Double(tfPrice.text ?? "0")!
		//pegar a gasolina pelo segment controll
		car.gasType = scGasType.selectedSegmentIndex
		
		if car._id == nil {
			RESTCar.save(car: car) { (success) in
				self.goBack()
			}
		}else{
			RESTCar.update(car: car) { (success) in
				print(success)
				self.goBack()
			}
		}
		
    }
	
	func goBack(){
		DispatchQueue.main.async {
			self.navigationController?.popViewController(animated: true)
		}
	}
	
	@objc func cancel(){
		tfBrand.resignFirstResponder()
	}
	
	@objc func done(){
		tfBrand.text = brands[pickerView.selectedRow(inComponent: 0)].fipe_name
		cancel()
	}
	
	func loadBrands(){
		RESTCar.loadBrands { (brands) in
			if let brands = brands {
				self.brands = brands.sorted(by: {$0.fipe_name < $1.fipe_name }) //ordenando de forma crescente
				DispatchQueue.main.async {
					self.pickerView.reloadAllComponents()
				}

			}
		}
	}

}

extension AddEditViewController: UIPickerViewDelegate, UIPickerViewDataSource {
	func numberOfComponents(in pickerView: UIPickerView) -> Int {
		return 1
	}
	
	func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
		return brands.count
	}
	
	func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
		
		let brand = brands[row]
		return brand.fipe_name
	}
}
